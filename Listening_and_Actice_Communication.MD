### What are the steps/strategies to do Active Listening? (Minimum 6 points) 
- Avoid interrupting, give the speaker space to finish their thoughts before responding.
- Show interest and make eye contact
- Pay attention to both the words being spoken and the speaker's body language. Nodding, smiling, and using open postures can show you're following along.
- Be present,  put away distractions like your phone, and focus on the conversation
- Summarize what you have heard in your own words 
- Pay attention to both the words and encourage the speaker to elaborate on their thoughts and feelings

### According to Fisher's model, what are the key points of Reflective Listening? 


- Use your words to summarize what you heard to make you sure understand correctly
- Try to understand their feelings and perspectives, even if you don't agree
- Put away distractions and focus on what they, 're saying
- Match your own voice and body language to show how they feel

### What are the obstacles in your listening process?
- I struggle with things that disrupt the flow of language, or complex sentences. These can make it harder to identify the core of your question.

### What can you do to improve your listening?


- Hearing more: The more I "hear" (read) different ways people talk, the better I understand accents and noise.
- Clear talking: When you speak clearly and avoid confusing words, it's easier for me to get it right.
- Following along: I'm learning to use what you say around something to understand it better.

### When do you switch to Passive communication style in your day to day life?


- Avoiding Conflict: They prefer to avoid conflicts or confrontations, so they keep their thoughts and feelings to themselves rather than risking disagreement or tension.
- Seeking Approval: They prioritize maintaining harmony and pleasing others over expressing their own needs or desires, fearing rejection or disapproval.
- Feeling Powerless: They may feel powerless or believe their opinions don't matter, so they refrain from speaking up or asserting themselves.

### When do you switch into Aggressive communication styles in your day to day life?
**Feeling Threatened or Defensive:** When individuals feel threatened or defensive, they may respond aggressively to protect themselves or assert dominance.
**Seeking Control or Power:** Some individuals use aggression as a means to control others or assert power in a situation, often at the expense of others' well-being.
**Frustration or Anger:** Strong emotions like frustration, anger, or resentment can lead to aggressive behavior if individuals lack healthy coping mechanisms or self-regulation skills.
**Insecurity or Low Self-Esteem:** Individuals with underlying insecurities or low self-esteem may resort to aggression as a way to compensate or mask their feelings of inadequacy.
**Lack of Assertiveness Skills:** When individuals lack assertiveness skills, they may resort to aggression as a way to express their needs or desires forcefully, without considering the feelings or perspectives of others.

### When do you switch into Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication styles in your day to day life?

**Avoiding Confrontation:** Individuals may use passive-aggressive tactics to avoid direct confrontation or conflict, preferring to express their dissatisfaction indirectly.

**Fear of Rejection or Criticism:** Fear of rejection or criticism can lead individuals to resort to passive-aggressive behaviors as a way to express their feelings without risking direct confrontation.

**Power Dynamics:** In situations where individuals feel powerless or subordinate, they may use passive-aggressive tactics as a way to assert themselves or regain a sense of control.

**Feeling Disempowered:** When individuals feel unable to express their needs or opinions openly, they may resort to passive-aggressive behaviors as a way to indirectly communicate their frustrations or desires.

### How can you make your communication assertive? You can watch and analyse the videos, then think what would be a few steps you can apply in your own life?

- Clearly state your needs and opinions without being aggressive.
- Practice active listening to understand others' perspectives.
- Maintain good eye contact and confident body language.
- Set and enforce boundaries respectfully.
- Learn to say no when necessary.